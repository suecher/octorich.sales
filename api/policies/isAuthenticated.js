/**
 * Created by Administrator on 2017/5/31.
 */


module.exports = function(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    else{
        return res.redirect('/login');
    }
};
