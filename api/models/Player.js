/**
 * Player.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        //ID 游戏ID
        Identification: {
            type: "string"
        },
        //头像URL
        portrait:{
            type:'string'
        },
        gameAccess:{
            type: "string"
        },
        gameId: {
            model: "games"
        },
        name: {
            type: "string"
        },
        mobile: {
            type: "string"
        },
        weixin: {
            type: "string"
        },
        qq: {
            type: 'string'
        },
        otherAccount: {
            type: "string"
        }
    }
};
