/**
 * SalespersonInStock.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

//销售人员库存卡片
module.exports = {
    attributes: {
        userId: {
            model: "user"
        },
        gameId: {
            model: "games"
        },
        channelId: {
            type:'integer',
            defaultsTo:0
        },
        agentId:{
            type:'integer',
            defaultsTo:0
        },
        count:{
            type:'integer',
            defaultsTo:0
        }
    }
};
