/**
 * Guild.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        //公会名称
        guildname: {
            type: "string"
        },
        //公会描述
        describe: {
            type: "string"
        },
        //logo图片地址
        logo: {
            type: "string"
        },
        //公会广告
        advert: {
            type: "string"
        },
        //公会通知，最多保存10条
        notices: {
            type: "array"
        },
        //公会类型
        type: {
            type: "string",
            //temporary 临时公会, static 长期公会
            enum: ['temporary', 'static']
        },
        //公会积分 备用
        point: {
            type: "integer",
        },
        //会长
        president: {
            model: 'user'
        },
        //相关配置
        config: {

        },
        //牌桌水印图片
        tableimg:{
            type: "string"
        },
        state:{
            type:"string",
            enum:['norma','disabled','shutoff','expired']
            //1.正常（normal） 2.停用 (disabled) 3.查封 (shutoff) 4.过期 (expired)
        }

    }
};
