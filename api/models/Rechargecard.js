/**
 * Rechargecard.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


//卡片充值记录

module.exports = {

  attributes: {
      gameId: {
          model: "games"
      },
      channelId: {
          type:'integer',
          defaultsTo:0
      },
      count:{
          type:'integer',
          defaultsTo:0
      },
      //代理ID号 比如 1000100
      agentId:{
          type:'integer'
      },
      userId: {
          model: "user"
      },
  }
};

