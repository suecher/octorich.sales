/**
 * Salesperson.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

//销售人员

module.exports = {

  attributes: {
      name:{
          type:'string'
      },
      gameId:{
          type:"string"
      },
      username:{
          type:'string'
      },
      password:{
          type:'string'
      },
      mobile:{
          type:'string'
      },
      //代理ID号 比如 1000100
      agentId:{
          type:'integer'
      },
      //用户类型 1.管理员 2.房卡销售 3.代理
      type:{
          type:'integer',
          defaultsTo:1
      }
  }
};
