
module.exports = {
    attributes: {
        //代理在系统中的ID
        userId: {
            model: "user"
        },
        //房间号
        roomNum:{
            type:'integer'
        },
        //代理游戏中的往返ID
        agentId:{
            type:'integer'
        }
    }
};

