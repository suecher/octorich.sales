/**
 * PlayerInGuild.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


//玩家与公会类

module.exports = {
    attributes: {
        //玩家ID
        playerId: {
            model: 'player'
        },
        //公会ID
        guildId: {
            model: 'guild'
        },
        //积分
        point:{
            type:'integer',
            defaultsTo:0
        },
        //虚拟货币
        money:{
            type:'integer',
            defaultsTo:0
        }
    }
};

