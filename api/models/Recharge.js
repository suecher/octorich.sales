/**
 * Sales.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        playerId: {
            model: 'player'
        },
        sum: {
            type: "integer",
            defaultsTo:0
        },
        channelId:{
            type: "integer",
            defaultsTo:0
        },
        gameId: {
            model: "games"
        },
        name: {
            type: "string"
        }
    }
};
