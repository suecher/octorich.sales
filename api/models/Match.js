/**
 * Match.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        //公会ID
        guildId:{
            model: 'guild'
        },
        //赛事时间
        matchName:{
            type:'string'
        },
        //赛事起始时间
        beginDate:{
            type:'date'
        },
        //赛事结束时间
        endDate:{
            type:'date'
        }
    }
};

