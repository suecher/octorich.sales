/**
 * Salescard.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


//卡片销售记录
module.exports = {
    attributes: {
        userId: {
            model: "user"
        },
        gameId: {
            model: "games"
        },
        channelId: {
            type: 'integer',
            defaultsTo: 0
        },
        count: {
            type: 'integer',
            defaultsTo: 0
        },
        //描述 比如 1.拨付房卡 2.长沙麻将开房
        describe:{
            type:'string'
        },
        //房号
        room:{
            type: 'integer',
        },
        //销售类型 1 房卡拨付产生销售 2,房间开设产生销售
        saletype: {
            type: 'integer',
            defaultsTo: 1
        },
        //玩家游戏中的账户
        gameAccess: {
            type: 'integer',
            defaultsTo: 0
        }

    }
};
