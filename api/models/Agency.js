/**
 * Agency.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        pId: {
            model: 'agency'
        },
        number: {
            type: "integer"
        },
        nickname: {
            type: "string"
        },
        describe: {
            type: "string"
        },
        //抽成比例
        rate:{
            type: 'integer',
            defaultsTo:0
        },
        permissionsId: {
            model: "permissions"
        },
        //是否使用，0为使用，1为停用
        ifuse:{
            type:'integer'
        }
    }
};
