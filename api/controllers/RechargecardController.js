/**
 * RechargecardController
 *
 * @description :: Server-side logic for managing rechargecards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

//充值卡片,管理平台充个值给代理商

var dbconfig = require('../../config/db');

//充值卡片记录
module.exports = {
    addcard:function(req,res){
        if(req.isAuthenticated()){
            var gameId = req.body.gameId;
            var channelId = req.body.channelId;
            var count = req.body.count;
            var mobile = req.body.mobile;
            var userId = "";
            if(gameId != undefined && channelId != undefined && count>0){


                User.findOne({mobile:mobile},function(err,userInfo){
                    if(err){
                        res.json({result: false, body: {errCode: "002", errMsg: "create rechargecard error"}});
                        return;
                    }

                    if(userInfo){

                        userId = userInfo.id;

                        Rechargecard.create({
                            gameId:gameId,
                            channelId:channelId,
                            count:count,
                            userId:userId
                        },function(err,rechargeInfo){
                            if(err){
                                res.json({result: false, body: {errCode: "002", errMsg: "create rechargecard error"}});
                                return;
                            }


                            //创建成功，给当前账户拨付卡片
                            //查找当前用户是否有卡片记录，如果存在就更新数据，如果没有就插入数据
                            SalespersonInStock.findOne({gameId:gameId,channelId:channelId,userId:userId},function(err,stockInfo){
                                if(err){

                                    email.sendWarningMail(
                                        "充值卡片时-查询卡片库存报错。",
                                        "错误警告模块:"+ "mongodb" + "\n" +
                                        "错误警告描述:"+ err + "",
                                        6
                                    );

                                    res.json({result: false, body: {errCode: "004", errMsg: "create recharge - find stock date error"}});
                                    return;
                                }

                                //判断是否找到相关记录
                                if(stockInfo != undefined){
                                    SalespersonInStock.update({id:stockInfo.id},{
                                        count:parseInt(stockInfo.count) + parseInt(count)
                                    },function(err,upStock){
                                        if(err){
                                            email.sendWarningMail(
                                                "充值卡片时成功后，更新库存数据出错。",
                                                "错误警告模块:"+ "mongodb" + "\n" +
                                                "错误警告描述:"+ err + "",
                                                6
                                            );

                                            res.json({result: false, body: {errCode: "005", errMsg: "create recharge - find stock date error"}});
                                            return;
                                        }
                                    })
                                } else {
                                    SalespersonInStock.create({gameId:gameId,channelId:channelId,userId:userId,count:count,agentId:userInfo.agentId},function(err,inStock){
                                        if(err){

                                            email.sendWarningMail(
                                                "充值卡片时成功后，创建卡片库存信息出错。",
                                                "错误警告模块:"+ "mongodb" + "\n" +
                                                "错误警告描述:"+ err + "",
                                                6
                                            );

                                            res.json({result: false, body: {errCode: "003", errMsg: "create recharge - find stock date error"}});
                                            return;
                                        }
                                    });
                                }
                            });

                            res.json({result:true,body:rechargeInfo});

                        });
                    } else {
                        res.json({result: false, body: {errCode: "006", errMsg: "User Not Exist"}});
                    }
                });




            } else {
                res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
            }
        } else {
            res.json({result: false, body: {errCode: "009", errMsg: "Permissions not Exist"}});
        }
    },
    //充金币
    addgold:function(req,res){
        var gameId = req.body.gameId;
        var channelId = req.body.channelId;
        var userId = req.body.userId;
        var sum = req.body.sum;

        if(userId && channelId != undefined && sum>0 && gameId != undefined){

            var dwUserID = userId;
            var dwScore  = sum;

            const sql = require('mssql');
            const config = {
                user:sails.config.mssql.username,
                password:sails.config.mssql.password,
                server:sails.config.mssql.host,
                database:sails.config.mssql.database,
                options: {
                    encrypt: true
                }
            };

            sql.connect(config, err => {
                if (err) {
                    if(err.name === 'ConnectionError'){
                        email.sendWarningMail(
                            "sql server connection to failed",
                            "错误警告模块:"+ "sql server connection" + "\n" +
                            "错误警告信息:"+ err.message+ "\n" +
                            "上下文源码:"+ err.ConnectionError + "\n" +
                            "错误警告描述:"+ err.ConnectionError + "\n"+ err.message + "\n" + err.code + "\n"+ err.name + "\n" + err.message + "\n",
                            6
                        );
                    }

                    return;
                }

                const request = new sql.Request();
                request.input('dwUserID', sql.Int, dwUserID);  //用户ID
                request.input('dwScore', sql.Int, dwScore);  //房卡数量;
                request.execute('WEB_GiveUserScore', function (err,recordesets, returnValue) {


                    sql.close();
                    //console.log(recordesets);
                    switch (recordesets.returnValue){
                        case  0:
                            //返回0代表充卡成功，需要扣除账户相应的卡片
                            res.json({result:true,body:recordesets.returnValue});
                            break;
                        case -1:
                            res.json({result:false,body:{errCode:"021",errMsg:"User Not Exist"}});
                            break;
                        default:
                            res.json({result:false,body:{errCode:"024",errMsg:"Unknown error"}});
                            break;
                    }
                });
            });

        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    //查询卡片充值记录
    getlist:function(req,res){
        var gameId = req.body.gameId;
        var channelId = req.body.channelId;
        var userId = req.body.userId;

        if(gameId != undefined && channelId != undefined && userId){
            Rechargecard.find({gameId:gameId,channelId:channelId,userId:userId},function(err,docs){

            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }

    }
};

