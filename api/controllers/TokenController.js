


/**
 * Created by Administrator on 2017/7/10.
 */


var https = require('https');

var sha1 = require('sha1');

var createNonceStr = function () {
    return Math.random().toString(36).substr(2, 15);
};

var createTimestamp = function () {
    return parseInt(new Date().getTime() / 1000) + '';
};

var raw = function (args) {
    var keys = Object.keys(args);
    keys = keys.sort();
    var newArgs = {};
    keys.forEach(function (key) {
        newArgs[key.toLowerCase()] = args[key];
    });

    var string = '';
    for (var k in newArgs) {
        string += '&' + k + '=' + newArgs[k];
    }
    string = string.substr(1);
    return string;
};


var appid = 'wxb909d7ff4b0acc76';
var appsecret = 'f5da940991a73ec9e561d562dfca1d42';


//生成签名
function generateSign(){

}

function getToken(callback){
    //判断最后一次调用token的时间戳是否存在 如果存在，判断是否过时，如不存在。直接调用微信API
    if(sails.config.expires_in_token){
        var time = (new Date().getTime() / 1000);

        //如果第一次请求的时间大于7000秒，重新获取 access_token
        if((time - sails.config.expires_in_token) > 7000){
            const https = require('https');

            https.get('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='+appid+'&secret='+appsecret, (res2) => {
                res2.on('data', (d) => {


                    var obj = JSON.parse(d.toString());
                    sails.config.access_token = obj.access_token;
                    sails.config.expires_in_token = new Date().getTime() / 1000;
                    callback({access_token:obj.access_token});

                    //process.stdout.write(d);
                });

            }).on('error', (e) => {
                console.error(e);
            });

        } else { //如果不大于直接获取当前值
            callback({access_token:sails.config.access_token});
        }
    } else {  //时间戳不存在调用微信API获取token

        console.log('开始获取token2');

        var tempt1 = new Date().getTime();
        const https = require('https');
        https.get('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='+appid+'&secret='+appsecret, (res2) => {
            res2.on('data', (d) => {
                console.log('token获取成功');
                console.log('用时:'+ (new Date().getTime() -tempt1));
                var obj = JSON.parse(d.toString());
                sails.config.access_token = obj.access_token;
                sails.config.expires_in_token = new Date().getTime() / 1000;
                callback({access_token:obj.access_token});
            });
        }).on('error', (e) => {
            console.error(e);
        });
    }
}

module.exports = {
    token:function(req,res){
        console.log(req.body);
        console.log(req.query);
        res.send(req.query.echostr);
    },
    getaccess_token:function(req,res){

    },
    //获取签名
//    signature:function(req,res){
//        var time = (new Date().getTime() / 1000);
//        if((time - sails.config.expires_in_ticket) > 7000){
//            const http = require('http');
//            http.get('http://'+sails.config.host+':'+sails.config.port+'/jsapi_ticket',(res2) => {
//                res2.on('data',(d) =>{
//                    var obj = d.toString();
//                    console.log(obj);
//                    //获得ticket 继续加密签名
//
//                });
//            }).on('error', (e) => {
//                console.error(e);
//            });
//        } else {
//
//            //获得ticket 继续加密签名
//            //sails.config.jsapi_ticket
//            console.log(sails.config.jsapi_ticket);
//        }
//
//        console.log(createTimestamp());
//        console.log(createNonceStr());
//
//        res.send('');
//    },

    signature:function(req,res){
        var page = req.query.page;
        var sign = {};
        console.log('请求签名');
        //获取token
        getToken(function(token){
            console.log('成功获取token');
            //判断ticket是否存在，如果存在，下一步再判断是否超时
            if(sails.config.expires_in_ticket){

                var time = (new Date().getTime() / 1000);
                //判断是否超时，如果第一次请求的时间大于7000秒，重新获取 access_token
                if((time - sails.config.expires_in_ticket) > 7000){
                    const https = require('https');
                    https.get('https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='+ token.access_token +'&type=jsapi', (res2) => {
                        res2.on('data', (d) => {
                            console.log(d);
                            var obj = JSON.parse(d.toString());
                            sails.config.jsapi_ticket = obj.ticket;
                            sails.config.expires_in_ticket = new Date().getTime() / 1000;


                            var sign_timestamp = parseInt(new Date().getTime() / 1000);

                            sign.ticket = obj.ticket;
                            sign.noncestr = sha1(new Date());
                            var string = 'jsapi_ticket=' + sign.ticket + '&noncestr=' + sign.noncestr + '&timestamp=' + sign.timestamp + '&url=' + page;
                            sign.timestamp = sign_timestamp;
                            console.log(string);
                            sign.signature = sha1(string);

                            res.json({ticket:obj.ticket});
                        });
                    }).on('error', (e) => {
                        console.error(e);
                    });
                } else { //如果不大于直接获取当前值
                    console.log('获取缓存的ticket');
                    var sign_timestamp = parseInt(new Date().getTime() / 1000);

                    sign.ticket = sails.config.jsapi_ticket;
                    sign.noncestr = sha1(new Date());
                    sign.timestamp = sign_timestamp;
                    var string = 'jsapi_ticket=' + sign.ticket + '&noncestr=' + sign.noncestr + '&timestamp=' + sign.timestamp + '&url=' + page;
                    console.log(string);
                    sign.signature = sha1(string);
                    res.json({ticket:sign.ticket,noncestr:sign.noncestr,timestamp:sign.timestamp,signature:sign.signature});
                }
            } else {
                console.log('第一次生成 ticket');
                const https = require('https');
                https.get('https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token='+ token.access_token +'&type=jsapi', (res2) => {
                    res2.on('data', (d) => {
                        console.log(d);
                        var obj = JSON.parse(d.toString());
                        sails.config.jsapi_ticket = obj.ticket;
                        sails.config.expires_in_ticket = new Date().getTime() / 1000;

                        var sign_timestamp = parseInt(new Date().getTime() / 1000);

                        sign.ticket = obj.ticket;
                        sign.noncestr = sha1(new Date());
                        sign.timestamp = sign_timestamp;
                        var string = 'jsapi_ticket=' + sign.ticket + '&noncestr=' + sign.noncestr + '&timestamp=' + sign.timestamp + '&url=' + page;
                        console.log(string);
                        sign.signature = sha1(string);
                        res.json({ticket:sign.ticket,noncestr:sign.noncestr,timestamp:sign.timestamp,signature:sign.signature});
                    });
                }).on('error', (e) => {
                    console.error(e);
                });
            }
        });
    }
};

