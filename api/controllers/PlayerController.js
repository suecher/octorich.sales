/**
 * PlayerController
 *
 * @description :: Server-side logic for managing players
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    selectPlayer: function (req, res) {
        var playerId = req.body.playerId;
        if (playerId) {
            Player.find({id: playerId}, function (err, doc) {
                if (err) {
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }
                res.json({result: true, body: doc});
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    }
};
