/**
 * TestController
 *
 * @description :: Server-side logic for managing tests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */





module.exports = {
    test1:function(req,res){
        if(req.isAuthenticated()){
            res.json({result:'登录成功'});
        } else {
            res.json({result:'没有登录'});
        }
    },
    test2:function(req,res){
        SalespersonInStock.find({},function (err,docs) {
            res.json(docs);
        });
    },
    test3:function(req,res){

        email.sendWarningMail(
            "测试新的日志邮件系统",
            "错误警告模块:"+ "mongodb" + "\n" +
            "错误警告描述:"+ '55' + "",
            6
        );

        res.json({});
    },
    test4:function(req,res){

        console.log(req.session);
        console.log(req.headers);
        console.log(req.user);
        console.log(req.userId);

        res.json(res.req.isAuthenticated());
    }
};
