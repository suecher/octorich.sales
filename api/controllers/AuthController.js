/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');

module.exports = {

    _config: {
        actions: false,
        shortcuts: false,
        rest: false
    },

    login: function(req, res) {

        //console.log((req.isAuthenticated()));

        passport.authenticate('local', function(err, user, info) {
            if ((err) || (!user)) {

//                return res.send({
//                    message: info.message,
//                    user: user
//                });

                return res.render('login',{error:"用户名或者密码错误"})
            }

            switch (user.type){
                //跳平台管理界面
                // case 1:
                //     req.login(user, function(err) {
                //         if (err) res.send(err);
                //         return res.render('admin_main',{userInfo:user,gameId:"59242b3da4bdd8f8136e2a4f"});
                //     });
                //     break;
                //跳房卡销售员
                case 2:
                    req.login(user, function(err) {
                        if (err) res.send(err);
                        return res.render('sales_main',{userInfo:user,gameId:"59242b3da4bdd8f8136e2a4f"});
                    });
                    break;
                //跳代理商
                // case 3:
                //     req.login(user, function(err) {
                //         if (err) res.send(err);
                //         return res.render('agency_main');
                //     });
                //     break;
            }

        })(req, res);
    },

    logout: function(req, res) {
        req.logout();
        res.redirect('/');
    }
};
