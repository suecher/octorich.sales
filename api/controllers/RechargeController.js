/**
 * SalesController
 *
 * @description :: Server-side logic for managing sales
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    selectSales: function (req, res) {
        var playId = req.body.playId;
        if (playId) {
            Recharge.find({playId: playId}, function (err, doc) {
                if (err) {
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }
                res.json({result: true, body: doc});
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    }
};

