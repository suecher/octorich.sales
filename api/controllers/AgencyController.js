/**
 * AgencyController
 *
 * @description :: Server-side logic for managing agencys
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    //创建代理商
    createAgency:function(req,res){
        var param=req.body;
        var username = param.username;
        var password = param.password;
        var pId=param.pId;
        var number=param.number;
        var nickname=param.nickname;
        var describe=param.describe;
        var mobile=param.mobile;
        var permissionsId=param.permissionsId;

        if (username && password) {
            Agency.create({username: username, password: password,pId:pId,number:number,nickname:nickname,
                describe:describe,mobile:mobile,permissionsId:permissionsId}, function (err, doc) {
                if (err) {
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    //代理商登录
    login: function (req, res) {
        var username = req.body.username;
        var password = req.body.password;
        if (username && password) {
            Agency.findOne({username: username, password: password}, function (err, doc) {
                if (err) {
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }

                if(doc){
                    res.json({result: true, body: doc});
                } else {
                    res.json({result: false, body: {errCode: "002", errMsg: "salesperson not exist"}});
                }

                res.json({result: true, body: doc});
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    //根据 Id 获取下级代理商
    getSalerById:function(req,res){
        var ById = req.body.ById;
        if (ById) {
            Agency.find({ById: ById}, function (err, doc) {
                if (err) {
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }
                res.json({result: true, body: doc});
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    //删除代理商
    delSaler:function(req,res){
        var id = req.body.id;
        if (id) {
            Agency.update({id: id},{type:1}, function (err, doc) {
                if (err) {
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }
                res.json({result: true, body: doc});
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    upSaler:function(req,res){

    }
};

