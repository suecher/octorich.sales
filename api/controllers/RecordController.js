/**
 * Created by Administrator on 2017/7/18.
 */


const sql = require('mssql');
var _ = require('lodash');


module.exports = {
    getRecord:function(req,res) {
        var dwOwnerID = req.body.agentId;
        if (dwOwnerID) {
            const config = {
                user: sails.config.mssql.username,
                password: sails.config.mssql.password,
                server: sails.config.mssql.host,
                database: sails.config.mssql.database,
                options: {
                    encrypt: true
                }
            };
            sql.connect(config, err => {
                if (err) {
                    if (err.name === 'ConnectionError') {
                    }
                    return;
                }
                const request = new sql.Request();
                request.input('dwOwnerID', sql.Int, dwOwnerID);  //代理商ID
                request.execute('WEB_GetGameRecord', function (err, recordesets, returnValue) {
                    sql.close();
                    //获取数据进行游戏分组。得出一局游戏的所有参赛人员
                    var result = _.groupBy(recordesets.recordset, function (n) {
                        return n.GameMainID;
                    });
                    var recordList = [];
                    //进行结构调整，生成客户端需要的数据结构。
                    for (item in result) {
                        recordList.push({gameMainID: result[item][0].GameMainID, list: result[item]});
                    }
                    switch (recordesets.returnValue) {
                        case  0:
                            //返回0代表充卡成功，需要扣除账户相应的卡片
                            res.json({result: true, body: recordList});
                            break;
                        case -1:
                            res.json({result: false, body: {errCode: "021", errMsg: "User Not Exist"}});
                            break;
                        default:
                            res.json({result: false, body: {errCode: "024", errMsg: "Unknown error"}});
                            break;
                    }
                });
            });
        }
        else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    }
};
