//临时房间

module.exports = {
    //创建临时房间
    createRoom:function(req,res){ //接口定义：optino={userId:开房人ID,roomNum:房间号}
        var agentId = req.query.agentId;
        var roomNum = req.query.roomNum;

        if(roomNum && agentId) {
            Room.create({agentId: agentId, roomNum: roomNum}, function (err, doc) {
                if (err) {
                    res.json({result: "false", errCode: "000", errMsg: "创建数据库条目错误"})
                    console.log(err)
                    return false
                }
                res.json({result: true})
            })
        } else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }
    },
    //确认执行
    checkroom:function(req,res){   //接口定义：option={roomNum:房间号，cardNum：扣卡数量}
        var param = req.query;

        console.log('接收到参数');
        console.log(req.query);

        if(param.roomNum && param.cardNum  != undefined){
            Room.findOne({roomNum:param.roomNum},function (err,ifroom) {
                if(err){
                    res.json({result:false,body:{err}});
                }
                else if(ifroom){
                    SalespersonInStock.findOne({agentId:ifroom.agentId},function (err,selectStock) {
                        if(err){
                            res.json({result:false,body:{err}});
                        }
                        else if(selectStock){
                            SalespersonInStock.update({agentId:ifroom.agentId},{count:selectStock.count-param.cardNum},function (err,checkStock) {
                                if(err){
                                    res.json({result:false,body:{err}});
                                }
                                else if(checkStock){
                                    res.json({result:true,body:{checkStock}});
                                }
                                else {
                                    res.json({result:false,body:{err:"删除库存失败"}});
                                }
                            });


                            //确认扣除房卡后将房间列表中的临时数据进行删除
                            Room.destroy({roomNum:param.roomNum},function (err,destroyRoom) {
                                if(err){
                                    //res.json({result:false,body:{err}});
                                }
                            });

                            SalesCardRecord.create({userId:selectStock.userId,gameId:1,channelId:0,describe:"代开房产生销售",count:param.cardNum,saletype:2,room:param.roomNum},function (err,doc) {
                                if(err){
                                    //res.json({result:false,body:{err:'充卡错误'}})
                                }
                            });

                        }
                        else {
                            res.json({result:false,body:{err:"库存信息不存在"}});
                        }
                    })
                }
                else {
                    res.json({result:false,body:{err:"临时房间不存在"}});
                }
            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }
    },
    //删除临时房间
    delroom:function(req,res) { //接口定义：option={roomNum:房间号}
        var param = req.query;
        if(param.roomNum){
            Room.findOne({roomNum:param.roomNum},function (err,selectRoom) {
                if(err){
                    res.json({result:false,body:{err}});
                }
                else if(selectRoom){
                    Room.destroy({roomNum:param.roomNum},function (err,destroyRoom) {
                        if(err){
                            res.json({result:false,body:{err}});
                        }
                        else if(destroyRoom){
                            res.json({result:true,body:{destroyRoom}});
                        }
                        else {
                            res.json({result:false,body:{err:"删除房间失败"}});
                        }
                    });
                }
                else {
                    res.json({result:false,body:{err:"房间不存在"}});
                }
            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }
    }
};
