/**
 * AdminController
 *
 * @description :: Server-side logic for managing admins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    sales:function(req,res){
        if(req.isAuthenticated()){
            switch (req.user.type){
                //跳平台管理界面
                case 1:
                    res.render('admin_main',{userInfo:req.user,gameId:"59242b3da4bdd8f8136e2a4f",agentId:req.user.agentId});
                    break;
                //跳房卡销售员
                case 2:
                    res.render('sales_main',{userInfo:req.user,gameId:"59242b3da4bdd8f8136e2a4f",agentId:req.user.agentId});
                    break;
                //跳代理商
                case 3:
                    res.render('agency_main');
                    break;
            }

        } else {
            res.render('login',{error:""});
        }
    },

    logout:function(req,res){
        req.logout();
        res.redirect('/login');
    }
};
