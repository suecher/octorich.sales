/**
 * SalescardController
 *
 * @description :: Server-side logic for managing salescards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */



module.exports = {
    //查询所有代理商的销售记录
    findCardByList: function () {
    },
    //按用户ID查询拨卡的销售数量
    findDialCardByUserID: function (req, res) {
        var userId = req.body.userId;
        var skip = req.body.skip;
        var limit = req.body.limit;
        SalesCardRecord.find({userId: userId, saletype: 1}).skip(skip * 5).limit(limit).exec(function (err, doc) {
            if (err) {
                console.log(err);
                res.json({result: false, errCode: 0, errMsg: err});
                return
            }
            SalesCardRecord.count({userId: userId, saletype: 1}).exec(function (err, num) {
                if (err) {
                    console.log(err);
                    res.json({result: false, errCode: 0, errMsg: err});
                    return
                }
                var userId = doc[0].playerId;
                for (var i = 1; i < doc.length; i++) {
                    userId += ",";
                    userId += doc[i].playerId
                }
                const sql = require('mssql');
                const config = {
                    user: sails.config.mssql.username,
                    password: sails.config.mssql.password,
                    server: sails.config.mssql.host,
                    database: sails.config.mssql.database,
                    options: {
                        encrypt: false
                    }
                };
                sql.connect(config, err => {
                    if (err) {
                        if (err.name === 'ConnectionError') {
                            email.sendWarningMail(
                                "sql server connection to failed",
                                "错误警告模块:" + "sql server connection" + "\n" +
                                "错误警告信息:" + err.message + "\n" +
                                "上下文源码:" + err.ConnectionError + "\n" +
                                "错误警告描述:" + err.ConnectionError + "\n" + err.message + "\n" + err.code + "\n" + err.name + "\n" + err.message + "\n",
                                6
                            );
                        }
                        return;
                    }
                    const request = new sql.Request();
                    request.input('listUserID', sql.Text, userId);
                    request.execute('WEB_GetListUserInfo', function (err, recordesets, returnValue) {
                        sql.close();
                        // console.log(recordesets.recordsets);
                        recordesets.recordsets[0].forEach(function (item) {
                            doc.forEach(function (bit) {
                                bit.playerId = parseInt(bit.playerId)
                                if (bit.playerId == item.UserID && !bit.plt) {
                                    bit.describe = item.NickName;
                                    bit.plt = 1;
                                    bit.LogoInfo = item.LogoInfo;
                                }
                            })
                        });
                        // console.log(doc)
                        res.json({result: true, body: doc, length: num})
                    });
                });
            });
        });
    },
    //按用户ID查询开房的销售数量
    findRoomCardByUserID: function (req, res) {
        var userId = req.body.userId;
        var skip = req.body.skip;
        var limit = req.body.limit;
        SalesCardRecord.find({userId: userId, saletype: 2}).skip(skip * 5).limit(limit).exec(function (err, doc) {
            if (err) {
                console.log(err);
                res.json({result: false, errCode: 0, errMsg: err});
                return
            }
            SalesCardRecord.count({userId: userId, saletype: 2}).exec(function (err, num) {
                if (err) {
                    console.log(err);
                    res.json({result: false, errCode: 0, errMsg: err});
                    return
                }
                res.json({result: true, body: doc, length: num})
            })
        })
    },
    //扣除卡片
    deductCard: function () {
        var gameId = req.body.gameId;
        var channelId = req.body.channelId;
        var count = req.body.count;
        var playId = req.body.playId;
        var userId = req.body.userId;
        var agentId = req.body.agentId;
        if (agent) {
            SalespersonInStock.findOne({
                gameId: gameId,
                channelId: channelId,
                userId: userId
            }, function (err, stockInfo) {
                if (err) {
                    email.sendWarningMail(
                        "检查卡片库存时，报错",
                        "错误警告模块:" + "mongodb" + "\n" +
                        "错误警告信息:拨付房卡时，查询库存出错\n" +
                        "错误警告描述:" + err + "\n",
                        6
                    );
                    res.json({
                        result: false,
                        body: {errCode: "004", errMsg: "create recharge - find stock date error"}
                    });
                    return;
                }
                //为空提示库存不足
                if (!stockInfo) {
                    res.json({result: false, body: {errCode: "005", errMsg: "sell card understock"}});
                    return;
                }
                //判断要拨付的卡片是否比库存多或者相等
                if (!(stockInfo.count >= count)) {
                    res.json({result: false, body: {errCode: "005", errMsg: "sell card understock"}});
                    return;
                }
                SalespersonInStock.update({id: stockInfo.id}, {
                    count: stockInfo.count - count
                }, function (err, upStock) {
                    if (err) {
                        email.sendWarningMail(
                            "拨付卡片后更新卡片库存报错",
                            "错误警告模块:" + "mongodb" + "\n" +
                            "造成的问题:卡片已经拨付，库存没扣除," +
                            "错误警告描述:" + err + "",
                            6
                        );
                        res.json({
                            result: false,
                            body: {errCode: "005", errMsg: "create recharge - find stock date error"}
                        });
                        return;
                    }
                });
                SalesCardRecord.create({
                    userId: userId,
                    channelId: channelId,
                    playerId: playId,
                    gameId: gameId,
                    count: count,
                    saletype: 1
                }, function (err, doc) {
                    if (err) {
                        res.json({result: false, body: {errCode: "002", errMsg: err}});
                        email.sendWarningMail(
                            "create sell card  date error",
                            "错误警告模块:" + "mongodb" + "\n" +
                            "错误警告信息:拨付房卡的时候出现错误导致拨付失败\n" +
                            "错误警告描述:" + err + "\n",
                            6
                        );
                        return;
                    }
                });
                res.json({result: true, body: recordesets.returnValue});
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    //拨付卡片（给玩家，产生销售）
    SellCard: function (req, res) {
        if (req.isAuthenticated()) {
            var gameId = req.body.gameId;
            var channelId = req.body.channelId;
            var count = req.body.count;
            var playId = req.body.playId;
            var userId = req.body.userId;
            if (gameId != undefined && channelId != undefined && count > 0 && playId && userId) {
                //先查询库存是否足够
                SalespersonInStock.findOne({
                    gameId: gameId,
                    channelId: channelId,
                    userId: userId
                }, function (err, stockInfo) {
                    if (err) {
                        email.sendWarningMail(
                            "检查卡片库存时，报错",
                            "错误警告模块:" + "mongodb" + "\n" +
                            "错误警告信息:拨付房卡时，查询库存出错\n" +
                            "错误警告描述:" + err + "\n",
                            6
                        );
                        res.json({
                            result: false,
                            body: {errCode: "004", errMsg: "create recharge - find stock date error"}
                        });
                        return;
                    }
                    //console.log(stockInfo)
                    //为空提示库存不足
                    if (!stockInfo) {
                        res.json({result: false, body: {errCode: "005", errMsg: "sell card understock"}});
                        return;
                    }
                    //判断要拨付的卡片是否比库存多或者相等
                    // 相等不能拨付？？
                    if (!(stockInfo.count >= count)) {
                        res.json({result: false, body: {errCode: "005", errMsg: "sell card understock"}});
                        return;
                    }
                    //console.log("拨付完毕");
                    var dwUserID = playId;
                    var dwPropCount = count;
                    const sql = require('mssql');
                    const config = {
                        user: sails.config.mssql.username,
                        password: sails.config.mssql.password,
                        server: sails.config.mssql.host,
                        database: sails.config.mssql.database,
                        options: {
                            encrypt: true
                        }
                    };
                    sql.connect(config, err => {
                        if (err) {
                            if (err.name === 'ConnectionError') {
                                email.sendWarningMail(
                                    "sql server connection to failed",
                                    "错误警告模块:" + "sql server connection" + "\n" +
                                    "错误警告信息:" + err.message + "\n" +
                                    "上下文源码:" + err.ConnectionError + "\n" +
                                    "错误警告描述:" + err.ConnectionError + "\n" + err.message + "\n" + err.code + "\n" + err.name + "\n" + err.message + "\n",
                                    6
                                );
                            }
                            return;
                        }
                        const request = new sql.Request();
                        request.input('dwUserID', sql.Int, dwUserID);  //用户ID
                        request.input('dwPropCount', sql.Int, dwPropCount);  //房卡数量;
                        //调用存储过程
                        request.execute('WEB_GiveUserRoomCard', function (err, recordesets, returnValue) {
                            //console.log(recordesets.returnValue);
                            sql.close();
                            switch (recordesets.returnValue) {
                                case  0:
                                    //返回0代表充卡成功，需要扣除账户相应的卡片
                                    SalespersonInStock.update({id: stockInfo.id}, {
                                        count: stockInfo.count - count
                                    }, function (err, upStock) {
                                        if (err) {
                                            email.sendWarningMail(
                                                "拨付卡片后更新卡片库存报错",
                                                "错误警告模块:" + "mongodb" + "\n" +
                                                "造成的问题:卡片已经拨付，库存没扣除," +
                                                "错误警告描述:" + err + "",
                                                6
                                            );
                                            res.json({
                                                result: false,
                                                body: {
                                                    errCode: "005",
                                                    errMsg: "create recharge - find stock date error"
                                                }
                                            });
                                            return;
                                        }
                                    });
                                    SalesCardRecord.create({
                                        userId: userId,
                                        gameId: gameId,
                                        channelId: channelId,
                                        describe: "玩家ID：" + playId,
                                        count: count,
                                        saletype: 1
                                    }, function (err, doc) {
                                        if (err) {
                                            res.json({result: false, body: {errCode: "002", errMsg: err}});
                                            email.sendWarningMail(
                                                "create sell card  date error",
                                                "错误警告模块:" + "mongodb" + "\n" +
                                                "错误警告信息:拨付房卡的时候出现错误导致拨付失败\n" +
                                                "错误警告描述:" + err + "\n",
                                                6
                                            );
                                            return;
                                        }
                                    });
                                    res.json({result: true, body: recordesets.returnValue});
                                    break;
                                case -1:
                                    res.json({result: false, body: {errCode: "021", errMsg: "User Not Exist"}});
                                    break;
                                default:
                                    res.json({result: false, body: {errCode: "024", errMsg: "Unknown error"}});
                                    break;
                            }
                        });
                    });
                });
            } else {
                res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
            }
        } else {
            res.json({result: false, body: {errCode: "009", errMsg: "Permissions not Exist"}});
        }
    },
    //查看一定时间内销售
    getSalesBydate: function (req, res) {
        var param=req.body;
        if(param.userId && param.startdate && param.enddate){
            var param=req.body;
            var query = SalesCardRecord.find();
            query.skip(param.skip*param.limit);
            query.limit(param.limit);
            query.where({userId:param.userId,createdAt:{'>':new Date(param.startdate),'<':new Date(param.enddate)}})
            var datecount=0;
            query.exec(function (err, docs) {
                if(err){
                    res.json({result:false,body:{message:"搜索销售记录错误"}});
                }
                else if(docs.length>0){
                    for(i=0;i<docs.length;i++){
                        datecount+=docs[i].count;
                    }
                    var dataList = {};
                    dataList.datecount = datecount;
                    dataList.list = docs;
                    res.json({result: true,body: dataList});
                }
                else {
                    res.json({result:true,body:{message:"没有销售记录"}});
                }
            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },
    getSalesCountBydate: function (req, res) {
        var param = req.body;
        if (param.userId && param.startdate && param.enddate) {
            var param = req.body;
            var query = SalesCardRecord.find();
            query.where({
                userId: param.userId,
                createdAt: {'>': new Date(param.startdate), '<': new Date(param.enddate)}
            })
            // query.skip(param.skip*param.limit);
            // query.limit(param.limit);
            var datecount = 0;
            query.exec(function (err, docs) {
                if (err) {
                    res.json({result: false, body: {message: "搜索销售记录错误"}});
                }
                else if (docs.length > 0) {
                    for (i = 0; i < docs.length; i++) {
                        datecount += docs[i].count;
                    }
                    // var dataList = {};
                    // dataList.datecount = datecount;
                    // dataList.list = docs;
                    res.json({result: true, body: datecount});
                }
                else {
                    res.json({result: true, body: {message: "没有销售记录"}});
                }
            })
        }
        else {
            res.json({result: false, body: {err: "缺少必要的参数"}});
        }
    }
};
