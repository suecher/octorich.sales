/**
 * SalespersonController
 *
 * @description :: Server-side logic for managing Salespeople
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */



module.exports = {
    //创建卡片销售人员
	create:function(req,res){
        if(req.isAuthenticated()){
            var username = req.body.username;
            var password = req.body.password;
            var name = req.body.name;
            var mobile = req.body.mobile;
            var type = req.body.type;   //1管理员2代理


            if(username && name  && password && mobile){
                User.findOne({mobile:mobile},function(err,userInfo){
                    if(err){
                        res.json({result: false, body: {errCode: "002", errMsg: err}});
                        return;
                    }
                    //如果用户不存在才可以注册新的用户
                    if(!userInfo){
                        User.create({
                            username:username,
                            password:password,
                            mobile:mobile,
                            type:type,
                            name:name
                        },function(err,doc){
                            if(err){
                                res.json({result: false, body: {errCode: "002", errMsg: err}});
                                return;
                            }
                            res.json({result: true, body:doc});
                        });
                    } else {
                        res.json({result: false, body: {errCode: "003", errMsg: "User Is Exist"}});
                    }
                });
            } else {
                res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
            }
        } else {
            res.json({result: false, body: {errCode: "009", errMsg: "Permissions not Exist"}});
        }
    },
    login:function(req,res){
        var username = req.body.username;
        var password = req.body.password;
        console.log(req.body)
        if(username && password){
            User.findOne({username:username,password:password},function(err,salesperson){
                console.log(salesperson)
                if(err){
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }

                if(salesperson){
                    res.json({result: true, body: salesperson});
                } else {
                    res.json({result: false, body: {errCode: "003", errMsg: "login error "}});
                }
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    //获取相关的销售信息
    getSalesInfo:function(req,res){
        var userId = req.body.userId;
        var gameId = req.body.gameId;
        var channelId = req.body.channelId;


        var sales_count = 0;
        var recharge_count = 0;
        var stock_count = 0;

        var cardInfo = {};

        //获取充值总数
        Rechargecard.find({userId:userId,gameId:gameId,channelId:channelId},function(err,rechargeCards){

            for(item of rechargeCards){
                recharge_count += item.count;
            }



            cardInfo.recharge_count = recharge_count;

            SalespersonInStock.findOne({userId:userId,gameId:gameId,channelId:channelId},function(err,stocks){


                if(stocks){
                    stock_count = stocks.count;
                }

                cardInfo.stock_count = stock_count;

                SalesCardRecord.find({userId:userId,gameId:gameId,channelId:channelId},function(err,record){


                    for(item of record){

                        sales_count += item.count;
                    }

                    cardInfo.sales_count = sales_count;

                    res.json(cardInfo);
                });

            });

        })
    },
    //获取代理id获取代理商的基本信息(下线玩家、销售数量、库存数量)
    getUserInfo:function(req,res){
        var param = req.body;
        if(param.userId){
            SalesCardRecord.find({userId:param.userId},function (err,doc) {
                if(err){
                    res.json({result:true,body:'查询玩家数量错误'});
                }
                else if(doc.length>0){
                    var customerNum=doc.length;   //代理下玩家数量
                    var salesNum=0;   //销售数量
                    var inventoryNum=0;   //库存数量
                    SalesCardRecord.find({userId:param.userId},function (err,stock) {
                        if(err){
                            res.json({result:false,body:'查询销售数据错误'});
                        }
                        else if(stock.length>0){
                            //console.log(stock)
                            for(i=0;i<stock.length;i++){
                                salesNum+=stock[i].count;
                            }
                            SalespersonInStock.find({userId:param.userId},function (err,inventory) {
                                if(err){
                                    res.json({result:false,body:'查询库存数据错误'});
                                }
                                else if(inventory.length>0){
                                    //console.log(inventory)
                                    inventoryNum=inventory[0].count;
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                    console.log(salesNum,inventoryNum,customerNum)
                                }
                                else {
                                    inventoryNum=0;
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                }
                            })
                        }
                        else {
                            salesNum=0;
                            SalespersonInStock.find({userId:param.userId},function (err,inventory) {
                                if(err){
                                    res.json({result:false,body:'查询库存数据错误'});
                                }
                                else if(inventory.length>0){
                                    inventoryNum=inventory[0].count
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                }
                                else {
                                    inventoryNum=0
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                }
                            })
                        }
                    })
                }
                else {
                    var customerNum=0;
                    var salesNum=0;
                    var inventoryNum=0;
                    SalesCardRecord.find({userId:param.userId},function (err,stock) {
                        if(err){
                            res.json({result:false,body:'查询销售数据错误'});
                        }
                        else if(stock.length>0){
                            for(i=0;i<stock.length;i++){
                                salesNum+=stock[i].count
                            }
                            SalespersonInStock.find({userId:param.userId},function (err,inventory) {
                                if(err){
                                    res.json({result:false,body:'查询库存数据错误'});
                                }
                                else if(inventory.length>0){
                                    inventoryNum=inventory[0].count
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                }
                                else {
                                    inventoryNum=0
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                }
                            })
                        }
                        else {
                            salesNum=0
                            SalespersonInStock.find({userId:param.userId},function (err,inventory) {
                                if(err){
                                    res.json({result:false,body:'查询库存数据错误'});
                                }
                                else if(inventory.length>0){
                                    inventoryNum=inventory[0].count
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                }
                                else {
                                    inventoryNum=0
                                    res.jsonp({customerNum:customerNum,salesNum:salesNum,inventoryNum:inventoryNum});
                                }
                            })
                        }
                    })


                }

            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }

        //res.jsonp({customerNum:102 玩家数量,salesNum:2837 销售数量,inventoryNum:273库存数量});
    },
    //获取代理基本信息
    selectUser: function (req, res) {
        var userId = req.body.userId;
        if (userId) {
            User.findOne({id: userId}, function (err, doc) {
                if (err) {
                    res.json({result: false, body: {errCode: "002", errMsg: err}});
                    return;
                }
                else if(doc){
                    res.json({result: true, body: doc});
                }
                else {
                    res.json({result: false, body: '获取代理信息失败'});
                }
            });
        } else {
            res.json({result: false, body: {errCode: "001", errMsg: "Missing Parameters"}});
        }
    },
    //修改代理商信息ok
    UpdateUser:function(req,res){
        var param = req.body;
        var obj = req.body.updateobj;
        //格式
        // {
        //"userId":"592f9d98a4bdd8f813772533",
        //"updateobj":{"name":"123"}
        //}
        if(param.userId){
            User.update({
                id:param.userId
            },obj,function(err,doc){
                if(err){
                    res.json({result:true,body:'修改数据错误'});
                }
                else if(doc){
                    res.json({result:true,body:doc});
                } else {
                    res.json({result:false,body:{err:"用户数据不存在,修改失败"}});
                }
            });
        } else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }
    },
    //修改密码ok
    UpdatePassword:function(req,res){
        var param = req.body;
        if(param.userId && param.password && param.OldPassword){
            User.findOne({id:param.userId,password:param.OldPassword},function (err,selectOldPassword) {
                if(err){
                    res.json({result:false,body:{err:"查询用户数据错误"}});
                }
                else if(selectOldPassword){
                    User.update({
                        id:param.userId
                    },{password:param.password},function(err,doc){
                        if(err){
                            res.json({result:false,body:{err:"查询用户数据错误"}});
                        }
                        else if(doc.length > 0){
                            res.json({result:true,body:doc});
                        } else {
                            res.json({result:false,body:{err:"用户数据不存在,修改失败"}});
                        }
                    });
                }
                else {
                    res.json({result:false,body:{err:"原密码错误"}});
                }
            });
        } else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }
    },
    //模糊搜索代理ok
    searchUser:function(request,response){
        var param=request.body;
        if(param.username ){
            var query = User.find();
            query.where({username:{'like':'%'+param.username+'%'}});
            //query.populate("userId");
            //query.sort('commentcount DESC');    //排序ASC正序,DESC反序
            query.skip(param.skip*param.limit);
            query.limit(param.limit);
            query.exec(function(err,doc){
                if(err){
                    response.json({result:false,body:'查询错误'});
                }else if(doc){
                    response.json({result:true,body:doc});
                }
                else {
                    response.json({result:false,body:'未查到用户信息'})
                }
            });
        }else{
            response.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },

};

