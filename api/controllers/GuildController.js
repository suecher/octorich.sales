/**
 * GuildController
 *
* @description :: Server-side logic for managing guilds
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
    */

module.exports = {
    //创建公会,ok
    createGuild:function(req,res){
        var param = req.body;
        if(param.guildname && param.type && param.president){
           Guild.findOne({president:param.president},function(err,selectGuild){
               if(err){
                   res.json({result:true,body:{message:"创建工会错误"}});
               }
               else if(selectGuild){
                   res.json({result:true,body:{message:"你已创建工会，不能重复创建"}});
               }
               else {
                   User.findOne({id:param.president},function (err,ifUser) {
                       if(err){
                           res.json({result:false,body:{err:"err,查询会长id出错"}});
                       }
                       else if (ifUser){
                           Guild.create({
                               guildname:param.guildname,   //工会名
                               type:param.type,            //工会类型//temporary 临时公会, static 长期公会
                               president:param.president,       //工会会长id
                               state : "norma",   //1.正常（normal） 2.停用 (disabled) 3.查封 (shutoff) 4.过期 (expired)
                               describe:"",
                               logo:"",
                               advert:"",
                               notices:[],
                               config:"",
                               tableimg:""
                           },function(err,doc){
                               if(err){
                                   res.json({result:false,body:err});
                               }
                               else if(doc){
                                   res.json({result:true,body:doc});
                               }
                               else {
                                   res.json({result:false,body:{err:"err,创建失败"}});
                               }
                           })
                       }
                       else {
                           res.json({result:false,body:{err:"err,会长id不存在"}});
                       }
                   })

               }

           })
        }
        else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }
    },
    //更改工会状态，ok //1.正常（normal） 2.停用 (disabled) 3.查封 (shutoff) 4.过期 (expired)
    updateStateGuild:function(req,res){
        var parm=req.body;
        if(parm.guildId && parm.state){
            Guild.findOne({id:parm.guildId},function(err,selectGuild){
                if(selectGuild){
                        Guild.update({id:parm.guildId},{state:parm.state},function(err,dissloution){
                            if(err){
                                res.json({result:false,body:{err:"更改出错"}});
                            }
                            else if(dissloution){
                                res.json({result:true,body:{message:dissloution}});
                            }
                            else {
                                res.json({result:false,body:{err:"更改失败"}});
                            }
                        })
                }
                else {
                    res.json({result:false,body:{err:"你要更改状态的工会不存在，请重新查询工会id"}});
                }
            })

        }
        else {
            res.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },
    //根据工会id查看工会信息,ok
    findGuild:function(req,res){
        var param=req.body;
        if (param.guildId){
            Guild.findOne({id:param.guildId},function(err,findGuild){
                if(err){
                    res.json({result:false,body:{err:"查看工会信息出错"}});
                }
                else if(findGuild){
                    res.json({result:true,body:{message:findGuild}});
                }
                else {
                    res.json({result:false,body:{err:"未找到该工会"}});
                }
            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },
    //更新公会信息，ok
    updateGuild:function(req,res){
        var param = req.body;
        var obj = req.body.updateobj;
        if(param.guildId){
            Guild.update({
                id:param.guildId
            },obj,function(err,doc){
                if(err){
                    res.json({result:true,body:'修改数据错误'});
                }
                else if(doc){
                    res.json({result:true,body:doc});
                } else {
                    res.json({result:false,body:{err:"部落数据不存在,修改失败"}});
                }
            });
        } else {
            res.json({result:false,body:{err:"缺少必要参数"}});
        }
    },
    //传值格式
    // {
    //     "guildId":"59ca482efeeaad38229e7b7b",
    //     "updateobj":{"guildname" : "小刀会"}
    // }
    //模糊搜索工会,OK
    searchGuild:function(req,res){
        var param=req.body;
        if(param.guildname ){
            var query = Guild.find();
            query.where({guildname:{'like':'%'+param.guildname+'%'}});
            //query.populate("userId");
            //query.sort('commentcount DESC');    //排序ASC正序,DESC反序
            query.skip(param.skip*param.limit);
            query.limit(param.limit);
            query.exec(function(err,doc){
                if(err){
                    res.json({result:false,body:'查询错误'});
                }else if(doc){
                    res.json({result:true,body:doc});
                }
                else {
                    res.json({result:false,body:'未查到用户信息'})
                }
            });
        }else{
            res.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },
};
