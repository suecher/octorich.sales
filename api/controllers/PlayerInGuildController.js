/**
 * PlayerInGuildController
 *
 * @description :: Server-side logic for managing Playeringuilds
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
//

module.exports = {
    //加入工会，ok
    joinGuild:function(req,res){
        var parm=req.body
        if(parm.gameAccess && parm.guildId){
            Guild.findOne({id:parm.guildId},function(err,selectGuild){
                if (selectGuild){
                    Player.findOne({gameAccess:parm.gameAccess},function(err,selectPlayer){
                        if(err){
                            res.json({result:false,body:{err}});
                        }
                        else if (selectPlayer){
                            PlayerInGuild.findOne({playerId:selectPlayer.id,guildId:parm.guildId},function (err,ifjoin) {
                                if(err){
                                    res.json({result:false,body:{err}});
                                }
                                else if(ifjoin){
                                    res.json({result:false,body:{err:'不能重复加入同一俱乐部'}});
                                }
                                else {
                                    PlayerInGuild.create({playerId:selectPlayer.id,guildId:parm.guildId},function(err,join){
                                        if(err){
                                            res.json({result:false,body:{err}});
                                        }
                                        else if(join){
                                            res.json({result:true,body:{message:join}});
                                        }
                                        else {
                                            res.json({result:false,body:{err:'加入不成功'}});
                                        }
                                    })
                                }
                            })
                        }
                        else {
                            Player.create({
                                Identification:parm.Identification,
                                portrait:parm.portraitr,
                                gameId:parm.gameId,
                                name:parm.name,
                                mobile:parm.mobile,
                                gameAccess:parm.gameAccess
                            },function(err,Createplayer){
                                if(Createplayer){
                                    PlayerInGuild.create({playerId:Createplayer.id,guildId:parm.guildId},function(err,join){
                                        if(err){
                                            res.json({result:false,body:{err}});
                                        }
                                        else if(join){
                                            res.json({result:true,body:{message:join}});
                                        }
                                        else {
                                            res.json({result:false,body:{err:'加入不成功'}});
                                        }
                                    })
                                }
                                else {
                                    res.json({result:false,body:{err:'用户创建不成功'}});
                                }
                            })
                        }
                    })
                }
                else {
                    res.json({result:false,body:{err:"俱乐部不存在,请重新查找"}});
                }
            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },

    //踢出公会，ok
    outplayer:function(req,res){
        var parm=req.body;
        if(parm.playerId && parm.guildId){
            Guild.findOne({id:parm.guildId},function(err,selectGuild){
                if(err){
                    res.json({result:false,body:'搜索工会出错'})
                }
                else if(selectGuild){
                    Player.find({id:parm.playerId},function(err,selectPlayer){
                        if(err){
                            res.json({result:false,body:'搜索玩家出错'})
                        }
                        else if(selectPlayer.length>0){
                            PlayerInGuild.destroy({guildId:parm.guildId,playerId:parm.playerId},function(err,outplayer){
                                if(err){
                                    res.json({result:false,body:'踢出失败'})
                                }else {
                                    res.json({result:false,body:outplayer})
                                }
                            })
                        }
                        else {
                            res.json({result:false,body:'没有搜到该用户'})
                        }
                    })
                }
                else {
                    res.json({result:false,body:'没有该工会'})
                }

            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },

    //查询工会成员，ok
    findplayer:function(req,res){
        var param=req.body;
        if(param.guildId){
            var param=req.body;
            var query = PlayerInGuild.find({guildId:param.guildId});
            query.populate("playerId");
            //console.log(query);
            query.skip(param.skip*param.limit);
            query.limit(param.limit);

            query.exec(function (err, docs) {
                if(err){
                    res.json({result:false,body:{message:"搜索工会错误"}});
                }
                else if(docs){

                    // async.forEach(docs,function(item,callback){
                    //     Player.findOne({id:docs.id}, function (err, friend) {
                    //         if (friend) {
                    //             item.isFriend = true;
                    //         } else {
                    //             item.isFriend = false;
                    //         }
                    //         callback();
                    //     });
                    // },function(err){
                    //     res.json({result:true,body:docs});
                    // });
                    res.json({result: true, body: docs});
                }
                else {
                    res.json({result:true,body:{message:"该部落没有人"}});
                }
            })
        }
        else {
            res.json({result:false,body:{err:"缺少必要的参数"}});
        }
    },
};

