/**
 * Created by UPC on 2017/10/4.
 */



var nodemailer = require('nodemailer');

module.exports = {
    sendWarningMail:function(sub,text,level){
        //判断是否大于或者等于发送邮件的配置级别。同时开启邮件功能
        if(level >= sails.config.waremail.level && sails.config.waremail.enable){
            var transporter = nodemailer.createTransport(sails.config.waremail.from);

            var mailOptions = {
                from: sails.config.waremail.from.auth.user, // 发送者
                to: sails.config.waremail.to, // 接受者,可以同时发送多个,以逗号隔开
                subject: sub, // 标题
                text: text, // 文本
                //html: '<h2>nodemailer基本使用:</h2><h3><a href="http://blog.csdn.net/zzwwjjdj1/article/details/51878392">http://blog.csdn.net/zzwwjjdj1/article/details/51878392</a></h3>'
            };

            transporter.sendMail(mailOptions, function (err, info) {
                if (err) {
                    console.log(err);
                    return;
                }
            });
        }
    }
};
