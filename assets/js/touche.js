var width = $("body").width();
		var left = parseInt($(".touch-f").css("margin-left"));
		var startX = 0; //touch初始的x值
		var numb = 0;  //滑动的距离
		var max = $(".touch").length-1;
			max = max*width*-1;
		var dom = document.getElementsByClassName("touch-f")[0];
		function touchSatrtFunc (evt){//触摸
			try{
				var touch = evt.touches[0]; //获取第一个触点
	            var x = Number(touch.pageX); //页面触点X坐标

	            startX = x; //记录初始X坐标
			}

			catch(e){
				alert('touchSatrtFunc：' + e.message);
			}
		}

		function touchMoveFunc (evt){//滑动
			try{
				evt.preventDefault(); //阻止触摸时浏览器的缩放、滚动条滚动等
                var touch = evt.touches[0]; //获取第一个触点
                var x = Number(touch.pageX); //页面触点X坐标
                numb = x - startX;//滑动的距离
                var newLeft = left+numb;
                if(numb > 0 && left!==0){
                	$(".touch-f").css("margin-left",newLeft)
                }else if(numb <0 && left !== max ){
                	$(".touch-f").css("margin-left",newLeft)
                }

			}

			catch(e){
				 alert('touchMoveFunc：' + e.message);
			}
		}

		function touchEndFunc(evt){//松开
			try{
				evt.preventDefault(); //阻止触摸时浏览器的缩放、滚动条滚动等
				if(numb > 0 && left!==0){
					if(numb>= width/3){
						left = left+width;
						$(".touch-f").css("transition","all .2s");
						$(".touch-f").css("margin-left",left);
						setTimeout(function(){
							$(".touch-f").css("transition","all 0s");
						},200)

					}else{
						$(".touch-f").css("transition","all .2s");
						$(".touch-f").css("margin-left",left);
						setTimeout(function(){
							$(".touch-f").css("transition","all 0s");
						},200)
					}
				}else if(numb <0 && left !== max ){
					numb = numb*-1
					if(numb>= width/3){
						left = left-width;
						$(".touch-f").css("transition","all .2s");
						$(".touch-f").css("margin-left",left);
						setTimeout(function(){
							$(".touch-f").css("transition","all 0s");
						},200)
					}else{
						$(".touch-f").css("transition","all .2s");
						$(".touch-f").css("margin-left",left);
						setTimeout(function(){
							$(".touch-f").css("transition","all 0s");
						},200)
					}
				}
				setTimeout(function(){
					if(left == max){
						$(".touch-f")[0].insertBefore($(".touch")[0],[$(".touch").length-1].nextSibling)
						left = left+width;
						$(".touch-f").css("margin-left",left);
					}else if(left == 0){
						$(".touch-f")[0].insertBefore($(".touch")[$(".touch").length-1],$(".touch")[0])
						left = left-width;
						$(".touch-f").css("margin-left",left);
					}
				},220)
			}

			catch(e){
				alert('touchEndFunc：' + e.message);
			}
		}

		function bindEvent (){ //绑定事件
			dom.addEventListener('touchstart', touchSatrtFunc, false);//触摸
			dom.addEventListener('touchmove', touchMoveFunc, false);//滑动
			dom.addEventListener('touchend', touchEndFunc, false);//松开
		};

		$(document).ready(function(){
			$(".touch").css("width",width);
			$(".touch").css("height",$("body").height());
			$(".touch").css("float","left");
			$(".touch-f").css("width",$(".touch").length*width);

			try {
                document.createEvent("TouchEvent");

                bindEvent(); //绑定事件
            }
            catch (e) {
                // alert("不支持TouchEvent事件！" + e.message);
            }
		})
