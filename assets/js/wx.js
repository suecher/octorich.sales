/**
 * Created by Administrator on 2017/07/21 0021.
 */


function asdce(obj) {
    wx.onMenuShareAppMessage({
        title: obj.title, // 分享标题
        desc: obj.desc, // 分享描述
        link: obj.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl: 'http://sales.octorich.com/320.png', // 分享图标
        type: 'link', // 分享类型,music、video或link，不填默认为link
        dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
        success: function () {
            // alert('分享朋友成功');
            // 用户确认分享后执行的回调函数
        },
        cancel: function () {
            // 用户取消分享后执行的回调函数
        }
    })

    wx.onMenuShareTimeline({
        debug: false,
        title:  obj.title, // 分享标题
        link: obj.link, // 分享链接
        imgUrl: 'http://sales.octorich.com/320.png', // 分享图标
        success: function () {
            // 用户确认分享后执行的回调函数
            // alert("分享成功");
        },
        cancel: function () {
            // 用户取消分享后执行的回调函数
            // alert("分享失败");
        },
        complete:function()
        {
            // alert("complete：接口调用完成时执行的回调函数，无论成功或失败都会执行。");
        }
    });
}

$(document).ready(function(){

    var ticket = "";
    var noncestr = "";
    var timestamp = "";
    var signature = "";


    $.ajax({
        type:"get",
        url:'/sign',
        data:{page:window.location.href},
        success:function(data){

            var ticket = data.ticket;
            var noncestr = data.noncestr;
            var timestamp = data.timestamp;
            var signature = data.signature;

            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                appId: 'wxb909d7ff4b0acc76', // 必填，公众号的唯一标识
                timestamp: timestamp, // 必填，生成签名的时间戳
                nonceStr: noncestr, // 必填，生成签名的随机串
                signature: signature,// 必填，签名，见附录1
                jsApiList: ['onMenuShareTimeline',
                    'onMenuShareAppMessage'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
            });

            window.onload=function(){

            };

            wx.ready(function(){
                // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
            });

            wx.error(function(res){

                // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
            });


            function Demo()
            {
                wx.checkJsApi({
                    jsApiList: ['onMenuShareTimeline'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
                    success: function(res) {
                        // alert(res);
                        debugger;
                        // 以键值对的形式返回，可用的api值true，不可用为false
                        // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
                    }
                });
            }



        },error:function(err){
            console.log(JSON.stringify(err))
        }
    });
});
