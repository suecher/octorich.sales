// plt  0是当选 1是复选

var game = [
	{

		"type":1,
		"name":"牛牛上庄",
		"kindId":10,
		"config":{
			"number":{
				"plt":0,
				"content":[
					{
						"name":"10局",
						"keyId":1
					},
					{
						"name":"20局",
						"keyId":2
					}
				]
			},
            "wGamePlayer":[
                {
                    "name" : "6人",
                    "count" : 6
                }
            ],
            "wBaseScore" : [
                {
                    "name" : "底分1/2",
                    "count" : 0x01
                },
                {
                    "name" : "底分2/4",
                    "count" : 0x02
                },
                {
                    "name" : "底分4/8",
                    "count" : 0x04
                },
                {
                    "name" : "底分5/10",
                    "count" : 0x05
                }
            ],
            "wDoubleRule" : [
                {
                    "name" : "牛牛x3",
                    "count" : 0x01
                },
                {
                    "name" : "牛牛x4",
                    "count" : 0x02
                }
            ],
            "wPushCount" : [
                {
                    "name" : "推注5倍",
                    "count" : 5
                },
                {
                    "name" : "推注10倍",
                    "count" : 10
                },
                {
                    "name" : "推注15倍",
                    "count" : 15
                },{
                    "name" : "推注20倍",
                    "count" : 20
                }
            ],
            "wSpecialKind" : [
                {
                    "name" : "顺子牛",
                    "count" : 0x01
                },
                {
                    "name" : "五花牛",
                    "count" : 0x02
                },
                {
                    "name" : "同花牛",
                    "count" : 0x04
                },
                {
                    "name" : "葫芦牛",
                    "count" : 0x08
                },
                {
                    "name" : "炸弹牛",
                    "count" : 0x10
                },
                {
                    "name" : "五小牛",
                    "count" : 0x20
                },
                {
                    "name" : "顺金牛",
                    "count" : 0x40
                }
            ]
		}
	},
	{
		"type":1,
		"name":"明牌抢庄",
		"kindId":11,
		"config":{
			"number":{
				"plt":0,
				"content":[
					{
						"name":"10局",
						"keyId":3
					},
					{
						"name":"20局",
						"keyId":4
					}
				]
			},
            "wGamePlayer":[
                {
                    "name" : "6人",
                    "count" : 6
                }
            ],
            "wBaseScore" : [
                {
                    "name" : "底分1/2",
                    "count" : 0x01
                },
                {
                    "name" : "底分2/4",
                    "count" : 0x02
                },
                {
                    "name" : "底分4/8",
                    "count" : 0x04
                },
                {
                    "name" : "底分5/10",
                    "count" : 0x05
                }
            ],
            "wDoubleRule" : [
                {
                    "name" : "牛牛x3",
                    "count" : 0x01
                },
                {
                    "name" : "牛牛x4",
                    "count" : 0x02
                }
            ],
            "wRobCount" : [
                {
                    "name" : "抢庄一倍",
                    "count" : 1
                },
                {
                    "name" : "抢庄两倍",
                    "count" : 2
                },
                {
                    "name" : "抢庄三倍",
                    "count" : 3
                },
                {
                    "name" : "抢庄四倍",
                    "count" : 4
                }
            ],
            "wPushCount" : [
                {
                    "name" : "推注5倍",
                    "count" : 5
                },
                {
                    "name" : "推注10倍",
                    "count" : 10
                },
                {
                    "name" : "推注15倍",
                    "count" : 15
                },{
                    "name" : "推注20倍",
                    "count" : 20
                }
            ],
            "wSpecialKind" : [
                {
                    "name" : "顺子牛",
                    "count" : 0x01
                },
                {
                    "name" : "五花牛",
                    "count" : 0x02
                },
                {
                    "name" : "同花牛",
                    "count" : 0x04
                },
                {
                    "name" : "葫芦牛",
                    "count" : 0x08
                },
                {
                    "name" : "炸弹牛",
                    "count" : 0x10
                },
                {
                    "name" : "五小牛",
                    "count" : 0x20
                },
                {
                    "name" : "顺金牛",
                    "count" : 0x40
                }
            ]
		}
	},
]

	function str2 (obj){
    //创建新的input框来区分新的参数
		var config = obj.config;

		var text = $("<span></span>").addClass("number"),
            text1 = $("<span></span>").addClass("wGamePlayer"),
            text2 = $("<span></span>").addClass("wBaseScore"),
            text3 = $("<span></span>").addClass("wDoubleRule"),
            text4 = $("<span></span>").addClass("wSpecialKind"),
            text5 = $("<span></span>").addClass("wRobCount"),
            text6 = $("<span></span>").addClass("wPushCount");
		if(config.number && config.number.plt==0){
			config.number.content.forEach(function(item){
                console.log(item)

				if( item.keyId == 2 || item.keyId == 4){
					var str = '<input data-keyId="'+item.keyId+'" type="radio" class="mgr mgr-success"  name="number" checked><p>'+item.name+'(x6)</p>';
					text.append($(str))
					i++;
					return;
				}
				var str = '<input data-keyId="'+item.keyId+'" type="radio" class="mgr mgr-success"  name="number"><p>'+item.name+'(x3)</p>';
				text.append($(str))
			})
		};
        if(config.wBaseScore){
            config.wBaseScore.forEach(function(item){
                if( item.count == 2){
                    var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wBaseScore" checked><p>'+item.name+'</p>';
                    text1.append($(str))
                    i++;
                    return;
                }
                var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wBaseScore"><p>'+item.name+'</p>';
                text1.append($(str))
            })
        };
        if(config.wGamePlayer){
            var i = 0;
            config.wGamePlayer.forEach(function(item){
                if( i == 0){
                    var str = '<input data-count="'+item.count+'" class="mgr mgr-success"  type="radio"  name="wGamePlayer" checked><p>'+item.name+'</p>';
                    text2.append($(str))
                    i++;
                    return;
                }
                var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wGamePlayer"><p>'+item.name+'</p>';
                text2.append($(str))
            })
        };
        if(config.wDoubleRule){
            var i = 0;
            config.wDoubleRule.forEach(function(item){
                if( i == 0){
                    var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wDoubleRule" checked><p>'+item.name+'</p>';
                    text3.append($(str))
                    i++;
                    return;
                }
                var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wDoubleRule"><p>'+item.name+'</p>';
                text3.append($(str))
            })
        };
        if(config.wRobCount){
            var i = 0;
            config.wRobCount.forEach(function(item){
                if( i == 0){
                    var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wRobCount" checked><p>'+item.name+'</p>';
                    text5.append($(str))
                    i++;
                    return;
                }
                var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wRobCount"><p>'+item.name+'</p>';
                text5.append($(str))
            })
        };
        if(config.wPushCount){
            config.wPushCount.forEach(function(item){
                if( item.count == 10){
                    var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wPushCount" checked><p>'+item.name+'</p>';
                    text6.append($(str))
                    i++;
                    return;
                }
                var str = '<input data-count="'+item.count+'" type="radio" class="mgr mgr-success" name="wPushCount"><p>'+item.name+'</p>';
                text6.append($(str))
            })
        };
        if(config.wSpecialKind){
            config.wSpecialKind.forEach(function (item) {
                var str = '<input data-count="'+item.count+'" type="checkbox" class="mgc mgc-success" name="wSpecialKind" checked><p>'+item.name+'</p>';
                text4.append($(str));
                return
            })
        }
		return [text,text1,text2,text3,text4,text5,text6]
	}

	function chek2c (obj){
		$(".action1").removeClass("action1");
		$(obj).addClass("action1");
		$("#chek3").text("");
		var kindId = $(obj).attr("data-kindId")
		game.forEach(function(item){
			if(item.kindId == kindId){
				var str = str2(item);
				str.forEach(function(item){
					$("#chek3").append(item);
				})
//				$("#chek2").append($(str));
				}
			})
        // console.log($('input'))
        // //input样式美化
        // $('input').iCheck({
        //     checkboxClass: 'icheckbox_polaris',
        //     radioClass: 'iradio_polaris',
        //     increaseArea: '-10%' // optional
        // });
	}
$(document).ready(function(){


	$(".classify").removeClass("action");
	$(".classify").eq(0).addClass("action");
	$("#chek2").text("");
	$("#chek3").text("");
	var type = 1;
	game.forEach(function(item){
		if(item.type == type){
			var str = str1(item);
			$("#chek2").append($(str));
		}
	})


	function str1 (obj){
		var str = '<div data-kindId="'+obj.kindId+'" class="classify" onclick="chek2c(this)">'+obj.name+'</div>';
		return str
	}

	$("#chek1>span").click(function chek2c (){
		$(".classify").removeClass("action");
		$(this).addClass("action");
		$("#chek2").text("");
		$("#chek3").text("");
		var type = $(this).index()+1;

		game.forEach(function(item){
			if(item.type == type){
				var str = str1(item);
				$("#chek2").append($(str));
			}
		})
	})



})
