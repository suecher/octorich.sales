/**
 * Created by Administrator on 2017/7/12.
 */

module.exports = {
    mssql:{
        host:'192.168.1.240',
        username:'sa',
        password:'Admin123!@#',
        database:'WHTreasureDB'
    },
    redis:{

    },
    mongodb:{
        adapter: 'sails-mongo',
        host: 'localhost', // defaults to `localhost` if omitted
        port: 27017, // defaults to 27017 if omitted
        database: 'scanhome-test' // or omit if not relevant
    },
    local:{
        host: "192.168.31.101",
        port: process.env.PORT || 1338,
    },

    //微信配置
    access_token:"",
    expires_in_token:0,
    jsapi_ticket:"",
    expires_in_ticket:0,
        
    //警告邮件配置
    waremail:{
        //服务器信息,以及授权码
        from:{
            service: '163',
            auth: {
                user: 'octorich@163.com',
                pass: 'Internet4431223' //授权码,通过QQ获取

            }
        },
        //接收者列表
        to:'suecher@163.com,313753445@qq.com',
        //发送email的级别
        level:4,
        //是否开启警告邮件功能
        enable:true
    }

};
